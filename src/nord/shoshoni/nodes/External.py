"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Node as Node
import nord.nord.Property as Property
from nord.nord.exceptions import RuntimeException as RuntimeException
from nord.shoshoni.utils import get_module_contents
import sys


class External(Node):
    """
    External Map elements reference external python modules.

    Allowing them to be utilized by the current graph. Similar to pythons "import"
    functionality, but the contents of the module will become Nodes which can be called
    or accessed within the Using or descendent containers.

    """
    def __init__(s, graph):
        super().__init__(graph)
        s._properties = {
            'modname': Property(s,
                                'modname',
                                description=_("The python importable "  # noqa: F821
                                              "module name to import.")),
            'menuicon': Property(s,
                                 'menuicon',
                                 default='unknown',
                                 description=_("The name of the icon to use when "  # noqa: F821
                                               "Use'ing this external module in another Map"))
        }

    def get_node(self, nid, runtime):
        """
        Get the requested itemid from the loaded module.

        This assumes that the ExternalUse.apply method has been called already.
        """
        node = runtime.map.get_node(nid)
        if node is None:
            raise RuntimeException(_("Requested Node with id: {} does not exist in {}")  # noqa: F821
                                   .format(nid, self.get_property('modname')))
        else:
            return node

    def handle_visible_request(self, data):
        """
        Communicate with the external visible by
        processing the request articulated by the
        dictionary data. The returned dictionary
        will be converted to JSON and shipped to
        the requesting visible.
        """
        rv = {}
        if "method" in data:
            if data["method"] == "get_module_contents" and "modname" in data:
                rv = get_module_contents(data["modname"])
        return rv


sys.modules[__name__] = External
