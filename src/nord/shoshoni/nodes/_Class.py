"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Receiver as Receiver
from importlib import import_module as use
import sys


class Class(Receiver):
    """The wrapper around python classes from within modules."""

    # save attrs allows "_" prefixed variables to be saved to the map file
    # yet, not be processed by any other methods which ignore "_*" attributes.
    _save_attrs = ["_implementation", "_definition", "_parent_module"]

    def __init__(self, graph):
        """Initialize the class reference with the module attribute."""
        super().__init__(graph)
        self.value = None
        self._implementation = None
        self._definition = None
        self._parent_module = None

    def get_instance(self):
        """Realize the implementation and definition, if not done, and return it."""
        if self._implementation is None:
            mod = use(self._parent_module)
            self._implementation = getattr(mod, self._definition)

        if self.has_args():
            args, kwargs = self.get_arg_values()
        else:
            args, kwargs = (), {}

        return self._implementation(*args, **kwargs)

    def set_arg(self, edge, value):
        """
        Convert the passed value to a python manageable structure.

        If there is no 'to_python' method of the value
        available, just send the value, and hope for the best...

        This suggests that extensions can be made Python compatable
        by implementing a to_python method on the nodes...

        This will be a data only feature. (or at least really should
        be so limitted)
        """
        if hasattr(value, 'to_python'):
            value = value.to_python()

        super().set_arg(edge, value)


sys.modules[__name__] = Class
