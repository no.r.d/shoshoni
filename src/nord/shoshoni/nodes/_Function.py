"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Receiver as Receiver
import nord.nord.utils.SpaceManager as use
import sys


class Function(Receiver):
    """"""

    # save attrs allows "_" prefixed variables to be saved to the map file
    # yet, not be processed by any other methods which ignore "_*" attributes.
    # In this case, we want to avoid _map_changes tracking modifications
    # to these fields???
    _save_attrs = ["_implementation", "_definition", "_parent_module"]

    def execute(self, runtime):
        """Realize the implementation and definition, if not done, and return it."""
        if not hasattr(self, '_implementation'):
            mod = use.module(self._parent_module)
            self._implementation = getattr(mod, self._definition)

        if self.has_args():
            args, kwargs = self.get_arg_values()
        else:
            args, kwargs = (), {}

        self.set_untracked_attr("value", self._implementation(*args, **kwargs))

        self.flag_exe(runtime)


sys.modules[__name__] = Function
