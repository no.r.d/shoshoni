"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Rule as Rule
import sys
from nord.nord.exceptions import RuntimeException as RuntimeException


class ClassAssign(Rule):
    """
    The ClassAssign rule places the Data within the current container's memory space with the runtime
    for subsequent use
    """

    def apply(s):
        """
        Adds the Target Data node to the runtime's current container's namespace for subsequent usage

        The target node is assigned in the Rule constructor as is the runtime
        The current node in the runtime is the source Node (of the edge to the target)
        """
        if hasattr(s.edge.get_source(), 'value'):
            s.edge.set_cargo( s.edge.get_source().get_instance())
            return True
        else:
            raise RuntimeException(_("Class asked to assign, but has nothing to assign: {}").format(s.edge.get_source()))


sys.modules[__name__] = ClassAssign
