"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Prepare the python module being imported for dereferencing.
"""
import nord.nord.Rule as Rule
import nord.nord.Map as Map
import nord.nord.utils.SpaceManager as use
import nord.shoshoni.nodes._Class as Class
import nord.shoshoni.nodes._Function as Function
import nord.shoshoni.nodes._Module as Module
import nord.shoshoni.nodes._Variable as Variable
import sys
import inspect


class ExternalUse(Rule):
    """Add the attrs within the source container to the "namespace" of the target."""

    def _add(self, node, ref, karte):
        node.id = ref.itemid
        node.name = ref.referenced_item
        karte.set_node(ref.itemid, node)
        node.flag_exe(self.runtime)

    def add_function(self, ref, attr, module, karte, parent):
        """Dereference attr into a Function."""
        node = Function(karte)
        node._definition = ref.referenced_item
        node._parent_module = self.edge.get_source().get_property('modname')
        node.type = '_Function'
        self._add(node, ref, karte)

    def add_class(self, ref, attr, module, karte, parent):
        """Dereference attr into a Class."""
        node = Class(karte)
        node._definition = ref.referenced_item
        node._parent_module = self.edge.get_source().get_property('modname')
        node.type = '_Class'
        self._add(node, ref, karte)

    def add_module(self, ref, attr, module, karte, parent):
        """Dereference attr into a Module."""
        node = Module(karte)
        node.type = '_Module'
        self._add(node, ref, karte)

    def add_variable(self, ref, attr, module, karte, parent):
        """Dereference attr into a Variable."""
        node = Variable(karte)
        node.type = '_Variable'
        self._add(node, ref, karte)

    def mod_to_attrs(self, module, karte, parent=''):
        """Wrap the references to the module attrs."""
        nd = self.runtime.map.nodes
        for attr, ref in [(getattr(module, nd[x].referenced_item), nd[x]) for x in nd
                          if nd[x].__class__.__name__ == 'Reference' and
                          nd[x].get_property('parentid') == self.edge.get_source().id and
                          hasattr(nd[x], 'referenced_item')]:
            if callable(attr) and not inspect.isclass(attr):
                # this is a function
                self.add_function(ref, attr, module, karte, parent)
            elif callable(attr):
                # this is a class
                self.add_class(ref, attr, module, karte, parent)
            elif inspect.ismodule(attr):
                # this is a submodule
                self.add_module(ref, attr, module, karte, parent)
            else:
                # this is a member object or variable.
                self.add_variable(ref, attr, module, karte, parent)

    def apply(self):
        """
        Add the source container's contents to the edge cargo.

        Create a Map made up of the appropriate nodes for each of
        the objects within the named module. Link these attrs
        with all references within the running Map.
        """
        module = use.module(self.edge.get_source().get_property('modname'))
        karte = Map(None)
        karte.name = self.edge.get_source().get_property('modname')
        self.mod_to_attrs(module, karte)
        self.edge.set_cargo(karte)
        return True


sys.modules[__name__] = ExternalUse
