"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.utils.SpaceManager as use
from nord.nord.exceptions import UseException as UseException
import inspect


def get_module_contents(modname):
    try:
        # f = import_module(path)
        f = use.module(modname)
    except UseException as e:
        # Try to import the last element from the rest
        if '.' in modname:
            objname = modname.split('.')[-1]
            # f = import_module('.'.join(path.split('.')[:-1]))
            f = use.module('.'.join(modname.split('.')[:-1]))
            if hasattr(f, objname):
                f = getattr(f, objname)
            else:
                raise e
        else:
            raise e

    rv = {
        "functions": [],
        "classes": [],
        "modules": [],
        "variables": []
    }
    for nk in use.get_mod_subs(f):
        node = getattr(f, nk)

        # classes, modules and functions are handled
        # from imported modules.
        # modules generate submodules.
        # classes generate containers with their own special menu
        # functions and variables are added to the current level's menu as items.

        name = f"{modname}.{node}"
        if hasattr(node, '__name__'):
            rname = node.__name__
        else:
            rname = f"<{type(node)} {node}>"

        if callable(node) and not inspect.isclass(node):
            # this is a function
            rv["functions"].append({"title": name, "identifier": rname})
        elif callable(node):
            # this is a class
            rv["classes"].append({"title": name, "identifier": rname})
        elif inspect.ismodule(node):
            # this is a submodule
            rv["modules"].append({"title": name, "identifier": rname})
        else:
            # this is a member object or variable.
            rv["variables"].append({"title": name, "identifier": rname})

    return rv
