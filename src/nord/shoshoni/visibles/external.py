"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Class as module providing visualization for importing python modules.
"""

from nord.sigurd.utils.verboser import funlog
from nord.sigurd.utils.log import err, critical
from nord.sigurd.shell.visnode import VisNode
from nord.sigurd.shell.menuItem import MenuItem
from nord.sigurd.shell.submenu import SubMenu
from nord.sigurd.visibles.reference import Reference
import nord.sigurd.shell.vismap as vm
import nord.sigurd.utils.namegen as namegen
import nord.sigurd.shell.mainmenu as mainmenu
import nord.sigurd.managers.event as event


class External(VisNode):
    """Visualization to connect a python module to the current graph."""

    def __init__(self, uid, app):
        """Initialize the extenal visualization."""
        super().__init__(uid, 'external', app, space="shoshoni")
        self.menuItem = None

    def add_menu_item(self, identifier, menu_items, menuicon, name):
        """Add item to menu which adds a reference to the graph."""
        err("Adding external menu item: {}".format(identifier))

        def gen_callback(name, refid, menuicon):

            @event.callback
            @funlog
            def func(*args, **kwargs):
                """
                Add a node to the graph.

                It is a special node from the runtime perspective in that it
                references the selected container in the imported Map..


                ADD CODE TO add the item to the Screen...
                """
                reference = vm.create_node("Reference", args[0], space="nord")
                # critical("Called an click of: {} with id {}".format(name, refid))
                # reference = Reference(f"{name}:{namegen.get()}", self.app, menuicon)
                # om.add_item(args[0], self.app, reference)

                # The following have been moved to on_mapped_set_props
                # such that the switch to map driven UI changes will work...
                # reference.mapentry.referenced_item = name
                # reference.mapentry.set_property('parentid', self.mapentry.id)
                # reference.mapentry.set_property('itemid', refid)

                # def on_mapped_set_props():
                reference.referenced_item = name
                reference.set_property('parentid', self.mapentry.id)
                reference.set_property('itemid', refid)
                # reference.visualization.onMappedFuncs.append(on_mapped_set_props)

            return func

        nid = namegen.id()

        mi = MenuItem(menuicon,
                      gen_callback(identifier, nid, menuicon),
                      title=f"Add {self.mapentry.modname}.{name}")
        menu_items.append(mi)

    @funlog
    def add_function(self, node, menu_items, name):
        """Add the passed function reference to the menu."""
        self.add_menu_item(node, menu_items, 'action', name)

    @funlog
    def add_class(self, node, menu_items, name):
        """
        Add the passed class instance creator to the menu.

        The added class, when rendered, should have a menu,
        (maybe within it?) that renders it's methods and members.

        These then need a way to be accessed by edges outside the class.
        """
        self.add_menu_item(node, menu_items, 'container', name)

    @funlog
    def add_module(self, node, menu_items, name):
        """
        Add the submenu for the passed module to the menu.

        Basically, add a menuitem that opens a submenu,
        that is processed like the parent module. Recursively.
        """
        self.add_menu_item(node, menu_items, 'static', name)

    @funlog
    def add_variable(self, node, menu_items, name):
        """Add a reference creator for the passed member variable."""
        self.add_menu_item(node, menu_items, 'data', name)

    @funlog
    def apply(self):
        """
        Create a SubMenu and inject it into the mainmenu.

        Extract all the top level containers from the graph
        and add a menuItem for each of them to the SubMenu
        """
        if self.menuItem is not None:
            """Only establish the menu once."""
            return

        if self.mapentry.is_set_property('modname'):
            path = self.mapentry.get_property('modname')

            """
            Need a mapentry type capability to ask it to talk to its backend
            self, and interrogate the requested module.

            Then, given results in a format this module is good with
            loop through them and generate the requested menu

            so, who gets the wrigan dependency to facilitate the comms?

            Might need to be walujapi. and the Node or lower class. Whoever it is
            will need to be able to generate or access the entire map, so the
            graph can be updated on the backend.
            """
            request = {
                "method": "get_module_contents",
                "modname": path
            }
            self.mapentry_comm(request)
        else:
            err("External used by container, but no 'modules' set for the graph")

    def handle_mapentry_data(self, response):
        menu_items = list()
        icon_name = self.mapentry.get_property('menuicon')

        for itm in response["functions"]:
            self.add_function(itm["identifier"], menu_items, itm["title"])
        for itm in response["classes"]:
            self.add_class(itm["identifier"], menu_items, itm["title"])
        for itm in response["modules"]:
            self.add_module(itm["identifier"], menu_items, itm["title"])
        for itm in response["variables"]:
            self.add_variable(itm["identifier"], menu_items, itm["title"])

        if len(menu_items) > 1:
            submenu = SubMenu(icon_name, mainmenu, title=self.mapentry.modname)
            for mi in menu_items:
                submenu.add_item(mi, hide_on_click=True)
            mainmenu.add_item(submenu)
            self.menuItem = submenu
        elif len(menu_items) == 1:
            mainmenu.add_item(menu_items[0], hide_on_click=True)
            self.menuItem = menu_items[0]

    @funlog
    def revert(self):
        """Remove the injected submenu item from the mainmenu."""
        if self.menuItem is not None:
            mainmenu.rem_item(self.menuItem)
            self.menuItem.remove()
            del self.menuItem
            self.menuItem = None
