"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Map as Map
import json
from nord.nord import Runtime as RT
import nord.nord.utils.SpaceManager as use
from nord.nord.exceptions import RuntimeException as RuntimeException
import pytest
Runtime = RT.Runtime


def test_mapfile_property():
    use.add_location("dir://lib/python3.7/site-packages", "code")
    External = use.module('nord.shoshoni.nodes.External')
    from nord.nord.exceptions import PropertyException as PropertyException
    g = External(None)
    g.set_property('modname', '/dev/null')
    assert g.get_property('modname') == '/dev/null'

    try:
        g.get_property('modname')
    except PropertyException as e:
        assert str(e) == 'External has no property named: modname'

    try:
        g.is_set_property('modname')
    except PropertyException as e:
        assert str(e) == 'External has no property named: modname'

    try:
        g.check_property('modname')
    except PropertyException as e:
        assert str(e) == 'External has no property named: modname'

    assert g.check_property('modname')
    assert g.is_set_property('modname')

    g.apply({'id': 1, 'modname': 'bananas'})
    assert g.get_property('modname') == 'bananas'


@pytest.mark.skip(reason="BLOCKS BATCH TEST EXECUTION FROM ENDING")
def test_bad_imported_external(capsys):
    try:
        data = json.load(open("test/maps/datetime.n", "r"))
    except FileNotFoundError:
        data = json.load(open("shoshoni/test/maps/datetime.n", "r"))
    data['map']['nodes'][2]['itemid'] = 'DOESNOTEXIST'
    m1 = Map(data, verbose=True)
    try:
        m1.run(Runtime(), [])
        # captured = capsys.readouterr()
    except RuntimeException as e:
        assert str(e) == "Requested Node with id: DOESNOTEXIST does not exist in tests/maps/importme.n"

    try:
        data = json.load(open("test/maps/datetime.n", "r"))
    except FileNotFoundError:
        data = json.load(open("shoshoni/test/maps/datetime.n", "r"))

    del data['map']['nodes'][2]['parentid']
    m1 = Map(data, verbose=True)
    try:
        m1.run(Runtime(), [])
        # captured = capsys.readouterr()
    except RuntimeException as e:
        assert str(e) == "Reference not properly intialized"


@pytest.mark.skip(reason="BLOCKS BATCH TEST EXECUTION FROM ENDING")
def test_imported_external(capsys):
    try:
        data = json.load(open("test/maps/datetime.n", "r"))
    except FileNotFoundError:
        data = json.load(open("shoshoni/test/maps/datetime.n", "r"))
    m1 = Map(data, verbose=True)
    m1.run(Runtime(), [])
    captured = capsys.readouterr()
    assert captured.out == """Map created with 13 Nodes and 26 Edges

Executing node: Container:
    .id = c0b1d83d-1c27-4022-98bc-3dba988f5b63
    .name = Container: Under Viscous Hair
    .position = [1.5878485441207886, 0.0, -2.7787320613861084]
    .type = container
  Targets
     - Contains -> Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Contains -> Node(External): <id: 28f952fb-7cda-4fd3-bdc3-dabb5b21aefb, name: External: Friendly Flooded Ceiling>


Executing node: External:
    .id = 28f952fb-7cda-4fd3-bdc3-dabb5b21aefb
    .modname = datetime
    .name = External: Friendly Flooded Ceiling
    .position = [-2.4810118675231934, 0.0, -1.9227843284606934]
    .space = shoshoni
    .type = external
  Sources
     - Contains <- Node(Container): <id: c0b1d83d-1c27-4022-98bc-3dba988f5b63, name: Container: Under Viscous Hair>
  Targets
     - Use -> Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>


Executing node: Container:
    .id = 9a824fb2-8096-4fad-a616-0543c0a6aa39
    .name = Container: Friendly Viscous Slobber
    .position = [6.69873046875, 0.0, 1.6746826171875]
    .type = container
  Sources
     - Contains <- Node(Container): <id: c0b1d83d-1c27-4022-98bc-3dba988f5b63, name: Container: Under Viscous Hair>
     - Use <-!!-- Node(External): <id: 28f952fb-7cda-4fd3-bdc3-dabb5b21aefb, name: External: Friendly Flooded Ceiling>
  Targets
     - Contains -> Node(Builtin): <id: 429f9bc9-f7f1-4e1f-976e-e72f220d3085, name: Builtin: Super Dental Sparrow>
     - Contains -> Node(Convert): <id: e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4, name: Convert: Flat Flooded Laser>
     - Contains -> Node(Convert): <id: 7b85162c-a4a0-4014-be8d-3051fe11fbe8, name: Convert: Split Sublimated Mountain>
     - Contains -> Node(Convert): <id: 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce, name: Convert: Hard Scientific Cloud>
     - Contains -> Node(Data): <id: 51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa, name: Data: Smart Oderous Wall>
     - Contains -> Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>
     - Contains -> Node(Static): <id: a566cfad-494f-4667-9570-0439286f7269, name: Static: Gothic Squished Bag>
     - Contains -> Node(Static): <id: 7f281587-2207-47ed-9f56-1f67f27f8e71, name: Static: Hard Ranged Bag>
     - Contains -> Node(Static): <id: a57486cd-a262-49e6-bd1e-bb401a577fb6, name: Static: Hangry Forked Slobber>
     - Contains -> Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>

   ---> following: Use: [Node(External): <id: 28f952fb-7cda-4fd3-bdc3-dabb5b21aefb, name: External: Friendly Flooded Ceiling>]  --Use-->  [Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>]
Map created with 14 Nodes and 26 Edges

Executing node: Static:
    .id = a566cfad-494f-4667-9570-0439286f7269
    .name = Static: Gothic Squished Bag
    .position = [-27.8514347076416, -3.952014923095703, 8.865455627441406]
    .type = static
    .value = 2019
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
  Targets
     - Pass -> Node(Convert): <id: e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4, name: Convert: Flat Flooded Laser>


Executing node: Convert:
    .id = e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4
    .name = Convert: Flat Flooded Laser
    .position = [-17.304424285888672, -1.6411056518554688, 5.243081092834473]
    .space = iteru
    .type = convert
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Pass <-!!-- Node(Static): <id: a566cfad-494f-4667-9570-0439286f7269, name: Static: Gothic Squished Bag>
     - Cast <-!!-- Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>
  Targets
     - Pass -> Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>


Executing node: Static:
    .id = 7f281587-2207-47ed-9f56-1f67f27f8e71
    .name = Static: Hard Ranged Bag
    .position = [-28.893959045410156, -2.4534950256347656, 0.4836501181125641]
    .type = static
    .value = 02
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
  Targets
     - Pass -> Node(Convert): <id: 7b85162c-a4a0-4014-be8d-3051fe11fbe8, name: Convert: Split Sublimated Mountain>


Executing node: Convert:
    .id = 7b85162c-a4a0-4014-be8d-3051fe11fbe8
    .name = Convert: Split Sublimated Mountain
    .position = [-20.19044303894043, 0.43225860595703125, 0.9133983850479126]
    .space = iteru
    .type = convert
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Pass <-!!-- Node(Static): <id: 7f281587-2207-47ed-9f56-1f67f27f8e71, name: Static: Hard Ranged Bag>
     - Cast <-!!-- Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>
  Targets
     - Pass -> Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>


Executing node: Toint:
    .id = d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7
    .name = Toint: Gothic Nordic Cheese
    .position = [-29.194698333740234, 10.189581871032715, 4.576087951660156]
    .space = iteru
    .type = toint
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
  Targets
     - Cast -> Node(Convert): <id: e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4, name: Convert: Flat Flooded Laser>
     - Cast -> Node(Convert): <id: 7b85162c-a4a0-4014-be8d-3051fe11fbe8, name: Convert: Split Sublimated Mountain>
     - Cast -> Node(Convert): <id: 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce, name: Convert: Hard Scientific Cloud>


Executing node: Convert:
    .id = 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce
    .name = Convert: Hard Scientific Cloud
    .position = [-20.507883071899414, 0.7384033203125, -4.5947136878967285]
    .space = iteru
    .type = convert
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Pass <-!!-- Node(Static): <id: a57486cd-a262-49e6-bd1e-bb401a577fb6, name: Static: Hangry Forked Slobber>
     - Cast <-!!-- Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>
  Targets
     - Pass -> Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>


Executing node: Static:
    .id = a57486cd-a262-49e6-bd1e-bb401a577fb6
    .name = Static: Hangry Forked Slobber
    .position = [-29.318429946899414, -2.2191123962402344, -11.226203918457031]
    .type = static
    .value = 03
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
  Targets
     - Pass -> Node(Convert): <id: 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce, name: Convert: Hard Scientific Cloud>
     - Flow -> Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>


Executing node: Convert:
    .id = e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4
    .name = Convert: Flat Flooded Laser
    .position = [-17.304424285888672, -1.6411056518554688, 5.243081092834473]
    .space = iteru
    .type = convert
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Pass <-!!-- Node(Static): <id: a566cfad-494f-4667-9570-0439286f7269, name: Static: Gothic Squished Bag>
     - Cast <-!!-- Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>
  Targets
     - Pass -> Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>

   ---> following: Pass: [Node(Static): <id: a566cfad-494f-4667-9570-0439286f7269, name: Static: Gothic Squished Bag>]  --Pass-->  [Node(Convert): <id: e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4, name: Convert: Flat Flooded Laser>]
   ---> following: Cast: [Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>]  --Cast-->  [Node(Convert): <id: e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4, name: Convert: Flat Flooded Laser>]

Executing node: Reference:
    .id = 5073fffd-4c53-494e-9647-08cb09b435c2
    .itemid = 52bafd0d-b3c2-4ce0-8494-cbfacc61d32f
    .name = <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber
    .parentid = 28f952fb-7cda-4fd3-bdc3-dabb5b21aefb
    .position = [-6.934427261352539, 0.0, 1.3025307655334473]
    .referenced_item = datetime
    .type = reference
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Pass <-!!-- Node(Convert): <id: e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4, name: Convert: Flat Flooded Laser>
     - Pass <-!!-- Node(Convert): <id: 7b85162c-a4a0-4014-be8d-3051fe11fbe8, name: Convert: Split Sublimated Mountain>
     - Pass <-!!-- Node(Convert): <id: 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce, name: Convert: Hard Scientific Cloud>
     - Flow <-!!-- Node(Static): <id: a57486cd-a262-49e6-bd1e-bb401a577fb6, name: Static: Hangry Forked Slobber>
  Targets
     - Flow -> Node(Builtin): <id: 429f9bc9-f7f1-4e1f-976e-e72f220d3085, name: Builtin: Super Dental Sparrow>
     - Assign -> Node(Data): <id: 51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa, name: Data: Smart Oderous Wall>


Executing node: Convert:
    .id = 7b85162c-a4a0-4014-be8d-3051fe11fbe8
    .name = Convert: Split Sublimated Mountain
    .position = [-20.19044303894043, 0.43225860595703125, 0.9133983850479126]
    .space = iteru
    .type = convert
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Pass <-!!-- Node(Static): <id: 7f281587-2207-47ed-9f56-1f67f27f8e71, name: Static: Hard Ranged Bag>
     - Cast <-!!-- Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>
  Targets
     - Pass -> Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>

   ---> following: Pass: [Node(Static): <id: 7f281587-2207-47ed-9f56-1f67f27f8e71, name: Static: Hard Ranged Bag>]  --Pass-->  [Node(Convert): <id: 7b85162c-a4a0-4014-be8d-3051fe11fbe8, name: Convert: Split Sublimated Mountain>]
   ---> following: Cast: [Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>]  --Cast-->  [Node(Convert): <id: 7b85162c-a4a0-4014-be8d-3051fe11fbe8, name: Convert: Split Sublimated Mountain>]

Executing node: Convert:
    .id = 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce
    .name = Convert: Hard Scientific Cloud
    .position = [-20.507883071899414, 0.7384033203125, -4.5947136878967285]
    .space = iteru
    .type = convert
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Pass <-!!-- Node(Static): <id: a57486cd-a262-49e6-bd1e-bb401a577fb6, name: Static: Hangry Forked Slobber>
     - Cast <-!!-- Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>
  Targets
     - Pass -> Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>

   ---> following: Cast: [Node(Toint): <id: d2ebb8cd-a7bc-4e91-8842-bd94ec1c8bf7, name: Toint: Gothic Nordic Cheese>]  --Cast-->  [Node(Convert): <id: 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce, name: Convert: Hard Scientific Cloud>]
   ---> following: Pass: [Node(Static): <id: a57486cd-a262-49e6-bd1e-bb401a577fb6, name: Static: Hangry Forked Slobber>]  --Pass-->  [Node(Convert): <id: 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce, name: Convert: Hard Scientific Cloud>]

Executing node: Reference:
    .id = 5073fffd-4c53-494e-9647-08cb09b435c2
    .itemid = 52bafd0d-b3c2-4ce0-8494-cbfacc61d32f
    .name = <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber
    .parentid = 28f952fb-7cda-4fd3-bdc3-dabb5b21aefb
    .position = [-6.934427261352539, 0.0, 1.3025307655334473]
    .referenced_item = datetime
    .type = reference
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Pass <-!!-- Node(Convert): <id: e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4, name: Convert: Flat Flooded Laser>
     - Pass <-!!-- Node(Convert): <id: 7b85162c-a4a0-4014-be8d-3051fe11fbe8, name: Convert: Split Sublimated Mountain>
     - Pass <-!!-- Node(Convert): <id: 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce, name: Convert: Hard Scientific Cloud>
     - Flow <-!!-- Node(Static): <id: a57486cd-a262-49e6-bd1e-bb401a577fb6, name: Static: Hangry Forked Slobber>
  Targets
     - Flow -> Node(Builtin): <id: 429f9bc9-f7f1-4e1f-976e-e72f220d3085, name: Builtin: Super Dental Sparrow>
     - Assign -> Node(Data): <id: 51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa, name: Data: Smart Oderous Wall>

   ---> following: Pass: [Node(Convert): <id: e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4, name: Convert: Flat Flooded Laser>]  --Pass-->  [Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>]
   ---> following: Pass: [Node(Convert): <id: 7b85162c-a4a0-4014-be8d-3051fe11fbe8, name: Convert: Split Sublimated Mountain>]  --Pass-->  [Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>]
   ---> following: Pass: [Node(Convert): <id: 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce, name: Convert: Hard Scientific Cloud>]  --Pass-->  [Node(Reference): <id: 5073fffd-4c53-494e-9647-08cb09b435c2, name: <<class 'type'> <class 'datetime.datetime'>>:Fat Chipped Slobber>]
   ---> following (contained) : Contains: [Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>]  --Contains-->  [Node(Class): <id: 52bafd0d-b3c2-4ce0-8494-cbfacc61d32f, name: datetime>]**
   ---> following: Pass: [Node(Convert): <id: e4e44ee1-a319-4dfa-8758-7ff1dc6db3c4, name: Convert: Flat Flooded Laser>]  --Pass-->  [Node(Class): <id: 52bafd0d-b3c2-4ce0-8494-cbfacc61d32f, name: datetime>]**
   ---> following: Pass: [Node(Convert): <id: 7b85162c-a4a0-4014-be8d-3051fe11fbe8, name: Convert: Split Sublimated Mountain>]  --Pass-->  [Node(Class): <id: 52bafd0d-b3c2-4ce0-8494-cbfacc61d32f, name: datetime>]**
   ---> following: Pass: [Node(Convert): <id: 5bc9efd4-49a7-418f-9093-e2a9c5a8ddce, name: Convert: Hard Scientific Cloud>]  --Pass-->  [Node(Class): <id: 52bafd0d-b3c2-4ce0-8494-cbfacc61d32f, name: datetime>]**

Executing node: Data:
    .id = 51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa
    .name = Data: Smart Oderous Wall
    .position = [7.578783988952637, -0.248931884765625, 1.1726133823394775]
    .type = data
    .value = None
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Assign <-!!-- Node(Class): <id: 52bafd0d-b3c2-4ce0-8494-cbfacc61d32f, name: datetime>
  Targets
     - Pass -> Node(Builtin): <id: 429f9bc9-f7f1-4e1f-976e-e72f220d3085, name: Builtin: Super Dental Sparrow>

   ---> following: Assign: [Node(Class): <id: 52bafd0d-b3c2-4ce0-8494-cbfacc61d32f, name: datetime>]  --Assign-->  [Node(Data): <id: 51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa, name: Data: Smart Oderous Wall>]

Executing node: Builtin:
    .id = 429f9bc9-f7f1-4e1f-976e-e72f220d3085
    .name = Builtin: Super Dental Sparrow
    .position = [7.8437180519104, -0.6015663146972656, -8.064321517944336]
    .type = builtin
  Sources
     - Contains <- Node(Container): <id: 9a824fb2-8096-4fad-a616-0543c0a6aa39, name: Container: Friendly Viscous Slobber>
     - Pass <-!!-- Node(Data): <id: 51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa, name: Data: Smart Oderous Wall>
     - Flow <-!!-- Node(Class): <id: 52bafd0d-b3c2-4ce0-8494-cbfacc61d32f, name: datetime>

   ---> following: Pass: [Node(Data): <id: 51f71e25-b2a7-4f7c-8ac4-a9a77653d9fa, name: Data: Smart Oderous Wall>]  --Pass-->  [Node(Builtin): <id: 429f9bc9-f7f1-4e1f-976e-e72f220d3085, name: Builtin: Super Dental Sparrow>]
2019-02-03 00:00:00
"""
